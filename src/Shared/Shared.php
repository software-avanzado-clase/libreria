<?php
namespace SA\Shared;

/**
 * Esta clase es para uso compartido de tareas de los microservicios
 *
 * @package SA
 */
abstract class Shared
{
    /**
     * Esta función retorna un saludo al usuario
     *
     * @param string $nombre Nombre de la persona a saludar
     *
     * @return string
     */
    public static function saludar($nombre = 'world')
    {
        return 'Hello ' . $nombre;
    }

    /**
     * Despide a los usuarios
     *
     * @return string
     */
    public static function retornarDespedida()
    {
        return 'Bye world';
    }
}
